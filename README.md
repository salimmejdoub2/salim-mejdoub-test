SALIM MEJDOUB - REAL.DIGITAL-ARTCHITECTURE TEST
===
## Technology used
- Symfony 6
 
## Projects
- Transactions Project
- Voucher Project

## Requirements
- Php 8
- Mysql 8
- Symfony Cli
- Composer 2

## Install
- `cd transactions-project && composer install`
- `cd voucher-project && composer install`
- Change for each project Mysql Access credentials in Env File
- Import databses 

## Run two projects
- `cd transactions-project && symfony server:run` -> default : http://127.0.0.1:8000
- `cd voucher-project && Symfony server:run` -> default : http://127.0.0.1:8001

## Apis
- http://127.0.0.1:8000/api/v1/order : This will generate an order with random userId and random total of the order
- http://127.0.0.1:8000/api/v1/transaction : This will take last order and generate transaction with it, if the transaction is valid (paied) and the total of order > 100, the service will call the other project "voucher-project" to generate a voucher to return to this project to send it by mail to the client


#### Have a nice review - made with love by Salim Mejdoub  