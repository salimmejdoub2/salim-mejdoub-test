<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Transaction;
use App\Repository\OrderRepository;
use App\Utils\Random;
use App\Utils\Generator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Node\TransNode;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/v1', name: 'api')]
class ApiController extends AbstractController
{
    #[Route('/order', name: 'order', methods: ['GET'])]
    public function order(EntityManagerInterface $entityManager): JsonResponse
    {
        $dataArray = [
            "state" => "error",
            "code" => 304
        ];

        try {
            $userId = Random::UserId();
            $total = Generator::Number(1, 200);

            $order = new Order();
            $order->setTotal($total);
            $order->setUserId($userId);

            $entityManager->persist($order);
            $entityManager->flush();
            $dataArray["state"] = "success";
            $dataArray["code"] = 200;
        } catch (\Throwable $th) {
            $dataArray["message"] = $th->getMessage();
        }

        $response = new JsonResponse($dataArray);
        $response->setStatusCode($dataArray["code"]);

        return $response;
    }

    #[Route('/transaction', name: 'transaction', methods: ['GET'])]
    public function transaction(EntityManagerInterface $entityManager, OrderRepository $orderRepository): JsonResponse
    {
        $dataArray = [
            "state" => "error",
            "code" => 304
        ];

        try {
            $orders =  $orderRepository->findBy(
                [],
                ["id" => "DESC"]
            );
            if (sizeof($orders)) {
                $order = $orders[0];
                $transaction = new Transaction();
                $transaction->setReference(Random::Reference());
                $transaction->setRelatedOrder($order);
                $transaction->setState($order->getId() % 2 == 0 ? "valid" : "not-valid");
                $entityManager->persist($transaction);
                $entityManager->flush();
                if ($transaction->getRelatedOrder()->getTotal() > 100) {
                }
                $dataArray["state"] = "success";
                $dataArray["code"] = 200;
            }
        } catch (\Throwable $th) {
            $dataArray["message"] = $th->getMessage();
        }

        $response = new JsonResponse($dataArray);
        $response->setStatusCode($dataArray["code"]);

        return $response;
    }
}
