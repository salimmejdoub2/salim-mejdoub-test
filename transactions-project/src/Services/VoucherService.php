<?php

namespace App\Services;

class VoucherService
{

  public static function Generate(string $reference): string | bool
  {
    $result = false;
    try {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "http://127.0.0.1:8001/api/v1/voucher",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode([
          "reference" => $reference
        ]),
      ));
      $response = curl_exec($curl);
      if ($response) {
        $response = json_decode($response, true);
      }
    } catch (\Throwable $th) {
      throw new \Exception("Error Processing Request", 1);
    }

    return $result;
  }
}
