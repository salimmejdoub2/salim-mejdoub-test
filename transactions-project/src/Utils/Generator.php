<?php

namespace App\Utils;

class Generator
{

  public static function String(int $length = 10): string
  {
    $random = "";
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    for ($i = 0; $i < $length; $i++) {
      $random .= $characters[rand(0, $charactersLength - 1)];
    }

    return $random;
  }

  public static function Number(int $start = 10, int $end = 100): int
  {
    return rand($start, $end);
  }
}
