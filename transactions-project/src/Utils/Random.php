<?php

namespace App\Utils;

use \Datetime;

class Random
{
  public static function Reference()
  {
    $now = new DateTime();
    $reference = $now->getTimestamp() . '-' . Generator::String();
    return $reference;
  }

  public static function UserId()
  {
    return Generator::Number();
  }
}
