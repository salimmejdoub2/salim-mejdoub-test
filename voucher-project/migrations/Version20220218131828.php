<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220218131828 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE voucher ADD is_used TINYINT(1) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1392A5D8FEC530A9 ON voucher (content)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1392A5D8ED84D250 ON voucher (transaction_reference)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE messenger_messages CHANGE body body LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE headers headers LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE queue_name queue_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX UNIQ_1392A5D8FEC530A9 ON voucher');
        $this->addSql('DROP INDEX UNIQ_1392A5D8ED84D250 ON voucher');
        $this->addSql('ALTER TABLE voucher DROP is_used, CHANGE content content VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE transaction_reference transaction_reference VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
