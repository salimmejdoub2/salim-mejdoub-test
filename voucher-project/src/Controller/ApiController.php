<?php

namespace App\Controller;

use App\Entity\Voucher;
use App\Utils\Random;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/v1', name: 'api')]
class ApiController extends AbstractController
{
    #[Route('/voucher', name: 'voucher', methods: "POST")]
    public function voucher(Request $httpRequest, EntityManagerInterface $entityManager): JsonResponse
    {
        $dataArray  = [
            "state" => "error",
            "code" => 304
        ];
        try {
            $data = json_decode($httpRequest->getContent(), true);
            if (isset($data["reference"])) {
                $voucher = new Voucher();
                $voucher->setContent(Random::Reference(6));
                $voucher->setTransactionReference($data["reference"]);
                $voucher->setIsUsed(true);
                $entityManager->persist($voucher);
                $entityManager->flush();
                $dataArray["voucher"]  = $voucher->getContent();
                $dataArray["code"]  = 200;
            }
        } catch (\Throwable $th) {
            $dataArray["message"]  = $th->getMessage();
        }

        $response = new JsonResponse($dataArray);
        $response->setStatusCode($dataArray["code"]);
        return $response;
    }
}
